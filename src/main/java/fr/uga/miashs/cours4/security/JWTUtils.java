package fr.uga.miashs.cours4.security;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.JWTOptions;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.jwt.Claims;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.List;

@Singleton
public class JWTUtils {
    @Inject
    @ConfigProperty(name = "privatekey.path", defaultValue = "/jwtRS256.key")
    private  String privatekeyPath;

    private JWTAuth provider;

    @PostConstruct
    public void init() {
        String privatekey = readPemFile();
        provider = JWTAuth.create(null, new JWTAuthOptions()
                .addPubSecKey(new PubSecKeyOptions()
                .setAlgorithm("RS256")
                .setSecretKey(privatekey)
        ));
    }


    //
    public  String generateToken(String email, List<String> roles, int validity) {
        JsonArray groups = new JsonArray();
        roles.forEach( r -> groups.add(r));

        Calendar exp = Calendar.getInstance();
        exp.add(Calendar.SECOND, validity);

        JsonObject jwt = new JsonObject();
        jwt.put(Claims.sub.name(), email);
        jwt.put(Claims.exp.name(), exp.toInstant().getEpochSecond());
        jwt.put(Claims.groups.name(), groups);

        return provider.generateToken(jwt, new JWTOptions().setAlgorithm("RS256"));
    }

    //
    private String readPemFile(){
        StringBuilder sb = new StringBuilder(8192);
        try(BufferedReader is = new BufferedReader(
                new InputStreamReader(
                        JWTUtils.class.getResourceAsStream(privatekeyPath), StandardCharsets.US_ASCII))) {

            String line;
            while((line = is.readLine()) != null) {
                if(!line.startsWith("-")) {
                    sb.append(line);
                    sb.append('\n');
                }
            }

        }catch (IOException e){
            e.printStackTrace();
        }

        return  sb.toString();
    }


}

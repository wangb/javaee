package fr.uga.miashs.cours4.dao;

import fr.uga.miashs.sempic.model.rdf.SempicOnto;
import fr.uga.miashs.sempic.rdf.BasicSempicRDFStore;

import fr.uga.miashs.cours4.model.AppAlbum;
import fr.uga.miashs.cours4.model.AppPhoto;
import fr.uga.miashs.cours4.model.AppUser;
import fr.uga.miashs.sempic.rdf.Namespaces;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.StringWriter;
import java.util.List;

public class PhotosDao  extends GenericJpaRestDao<AppPhoto> {



    // Il y a une dépendance mutuelle entre AlbumController et UserController
    // une solution est d'injecter Instance<UserController> à la place de  UserController
    // puis utiliser get() pour obtenir la référence
    @Inject
    private Instance<AlbumsDao> albumsController;
    //@Inject
    private BasicSempicRDFStore rdfStore = new BasicSempicRDFStore();
    public PhotosDao() {
        super(AppPhoto.class);
    }



    public Response createPhoto(long albumId, AppPhoto g, UriInfo uriInfo) {
        AppAlbum owner = getEm().find(AppAlbum.class,albumId);
        if (owner==null) throw new NotFoundException();
        g.setOwner(owner);
        Response r = create(g,uriInfo);

        rdfStore.createPhoto(g.getId(), g.getOwner().getId(), g.getOwner().getOwner().getId());
        return r;
    }

    public String hasAuthor(long photoId, List<String> obj) {
        // pour s'assurer que la photo existe
        AppPhoto p = getEm().find(AppPhoto.class, photoId);
        if (p == null) {
            throw new NotFoundException();
        }
        Model m = ModelFactory.createDefaultModel();
        Resource photoR = m.createResource(Namespaces.getPhotoUri(photoId));
        obj.forEach( o -> photoR.addProperty(SempicOnto.hasAuthor, ResourceFactory.createResource(o)));
        rdfStore.saveModel(m);
        StringWriter sw = new StringWriter();
        m.write(sw);
        return sw.toString();
    }

    public String addEvent(long photoId, List<String> obj) {
        // pour s'assurer que la photo existe
        AppPhoto p = getEm().find(AppPhoto.class, photoId);
        if (p == null) {
            throw new NotFoundException();
        }
        Model m = ModelFactory.createDefaultModel();
        Resource photoR = m.createResource(Namespaces.getPhotoUri(photoId));
        obj.forEach( o -> photoR.addProperty(SempicOnto.wasTakenFor, ResourceFactory.createResource(o)));
        rdfStore.saveModel(m);
        StringWriter sw = new StringWriter();
        m.write(sw);
        return sw.toString();
    }

    public String addDate(long photoId, String date) {
        // pour s'assurer que la photo existe
        AppPhoto p = getEm().find(AppPhoto.class, photoId);
        if (p == null) {
            throw new NotFoundException();
        }
        Model m = ModelFactory.createDefaultModel();
        Resource photoR = m.createResource(Namespaces.getPhotoUri(photoId));
        photoR.addLiteral(SempicOnto.date, date);
        rdfStore.saveModel(m);
        StringWriter sw = new StringWriter();
        m.write(sw);
        return sw.toString();
    }

    public String addLocation(long photoId, String location) {
        // pour s'assurer que la photo existe
        AppPhoto p = getEm().find(AppPhoto.class, photoId);
        if (p == null) {
            throw new NotFoundException();
        }
        Model m = ModelFactory.createDefaultModel();
        Resource photoR = m.createResource(Namespaces.getPhotoUri(photoId));
        photoR.addLiteral(SempicOnto.location, location);
        rdfStore.saveModel(m);
        StringWriter sw = new StringWriter();
        m.write(sw);
        return sw.toString();
    }

    public String addTitle(long photoId, String title) {
        // pour s'assurer que la photo existe
        AppPhoto p = getEm().find(AppPhoto.class, photoId);
        if (p == null) {
            throw new NotFoundException();
        }
        Model m = ModelFactory.createDefaultModel();
        Resource photoR = m.createResource(Namespaces.getPhotoUri(photoId));
        photoR.addLiteral(SempicOnto.title, title);
        rdfStore.saveModel(m);
        StringWriter sw = new StringWriter();
        m.write(sw);
        return sw.toString();
    }

    public String addDepicts(long photoId, List<String> obj) {
        // pour s'assurer que la photo existe
        AppPhoto p = getEm().find(AppPhoto.class, photoId);
        if (p == null) {
            throw new NotFoundException();
        }
        Model m = ModelFactory.createDefaultModel();
        Resource photoR = m.createResource(Namespaces.getPhotoUri(photoId));
        obj.forEach( o -> photoR.addProperty(SempicOnto.depicts, ResourceFactory.createResource(o)));
        rdfStore.saveModel(m);
        StringWriter sw = new StringWriter();
        m.write(sw);
        return sw.toString();
    }

    public Response getPhotos(long albumId, UriInfo uriInfo, int limit, int offset) {
        EntityGraph graph = getEm().getEntityGraph("AppPhoto.photoOnly");
        TypedQuery<AppPhoto> q = getEm().createNamedQuery("AppPhoto.findByOwner", AppPhoto.class);
        // alternative : query directe dans le code
        //TypedQuery<AppAlbum> q = getEm().createQuery("SELECT a FROM AppPhoto a WHERE a.owner.id=:id", AppPhoto.class);
        q.setHint("javax.persistence.fetchgraph", graph);
        q.setParameter("id",albumId);
        return executeQuery(q,uriInfo,limit,offset);
    }


    public Response getPhoto(long albumId, UriInfo uriInfo, int limit, int offset, long photoId) {
        rdfStore.readPhoto(photoId);
        EntityGraph graph = getEm().getEntityGraph("AppPhoto.photoOnly");
        //TypedQuery<AppPhoto> q = getEm().createNamedQuery("AppPhoto.findByOwner", AppPhoto.class);
        // alternative : query directe dans le code
        TypedQuery<AppPhoto> q = getEm().createQuery("SELECT a FROM AppPhoto a WHERE a.owner.id=:id AND a.id=:photoId", AppPhoto.class);
        q.setHint("javax.persistence.fetchgraph", graph);
        q.setParameter("id",albumId);
        q.setParameter("photoId",photoId);
        return executeQuery(q,uriInfo,limit,offset);
    }




}

package fr.uga.miashs.cours4.dao;

import fr.uga.miashs.cours4.model.AppAlbum;
import fr.uga.miashs.cours4.model.AppUser;
import fr.uga.miashs.cours4.model.AppUserType;
import fr.uga.miashs.cours4.security.JWTUtils;

import javax.inject.Inject;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.security.enterprise.identitystore.Pbkdf2PasswordHash;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Path("/users")
public class UsersDao extends GenericJpaRestDao<AppUser> {

	@Inject
	private Pbkdf2PasswordHash hashAlgo;

	@Inject
	private JWTUtils jwtUtils;



	public UsersDao() {
		super(AppUser.class);
	}

	public Response getGroupMembers(long groupId, UriInfo uriInfo, int limit, int offset) {
		TypedQuery<AppUser> q = getEm().createNamedQuery("AppUser.membersOf", AppUser.class);
		q.setParameter("id",groupId);
		return executeQuery(q,uriInfo,limit,offset);
	}

	// Gestion de l'authentification des Users
	public Response authenticate(String email, String password) {
		TypedQuery<AppUser> q = getEm().createNamedQuery("AppUser.login", AppUser.class);

		q.setParameter("email", email);
		AppUser u = q.getSingleResult();

		if(!hashAlgo.verify(password.toCharArray(),u.getPasswordHash())){
			throw  new NotAuthorizedException("Utilisateur Inconnu !");
		}

		//
		List<String> roles = new ArrayList<>();
		roles.add("user");
		if (u.getUserType() == AppUserType.ADMIN) {
			roles.add("admin");
		}

		String jwt = jwtUtils.generateToken(u.getEmail(), roles, 7200);
		return Response.ok().entity(jwt).build();
	}


	// Création de compte utilisateur
	@Override
	public Response create(AppUser u, UriInfo uriInfo) {
		// On pourrait aussi vérifier la longueur et autres contraintes sur le password
		if (u.getPassword() != null) {
			u.setPasswordHash(hashAlgo.generate(u.getPassword().toCharArray()));
			// Pour ne pas renvoyer le password dans la réponse
			u.setPassword(null);
		}else  {
			// Pour être sûr que l'on envoie pas un hash
			u.setPasswordHash(null);
		}
		return super.create(u, uriInfo);
	}


	public Response deleteUser(long userId) {

		Query q = getEm().createNativeQuery("DELETE FROM APPUSER WHERE id=?1");
		q.setParameter(1, userId);
		q.executeUpdate();
		return Response.status(Response.Status.OK).build();
	}

}

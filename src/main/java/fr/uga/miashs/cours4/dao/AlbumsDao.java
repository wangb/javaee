package fr.uga.miashs.cours4.dao;

import fr.uga.miashs.cours4.model.AppAlbum;
import fr.uga.miashs.cours4.model.AppGroup;
import fr.uga.miashs.cours4.model.AppPhoto;
import fr.uga.miashs.cours4.model.AppUser;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

public class AlbumsDao  extends GenericJpaRestDao<AppAlbum> {

    // Il y a une dépendance mutuelle entre AlbumController et UserController
    // une solution est d'injecter Instance<UserController> à la place de  UserController
    // puis utiliser get() pour obtenir la référence
    @Inject
    private Instance<UsersDao> usersController;

    public AlbumsDao() {
        super(AppAlbum.class);
    }

    public Response getGroupAlbums(long groupId, UriInfo uriInfo, int limit, int offset) {
        //EntityGraph graph = getEm().getEntityGraph("AppAlbum.albumFull");
        TypedQuery<AppAlbum> q = getEm().createNamedQuery("AppAlbum.albumOf", AppAlbum.class);
        //q.setHint("javax.persistence.fetchgraph", graph);
        q.setParameter("id",groupId);
        return executeQuery(q,uriInfo,limit,offset);
    }

    public Response addPhotos(List<Long> ids, long albumId, UriInfo uriInfo) {
        // Comme l'égalité et le hashcode sont définis sur l'email
        // cela cause un select du membre dans tous les cas
        //AppUser membre = em.getReference(AppUser.class,userId);
        // la solution la plus efficace revient à faire un native insert
        // et traiter les éventuelles erreurs (duplicate, foreign key)
        Query q = getEm().createNativeQuery("INSERT INTO AppAlbum_AppPhoto(AppAlbum_id,Photos_id) VALUES(?,?)");
        q.setParameter(1,albumId);
        ids.forEach( photoId -> {
            q.setParameter(2,photoId);
            q.executeUpdate();
            // La aussi meme avec em.getReference, il y a chargement des membres
            //em.find(AppGroup.class,g.id).getPhotos().add(membre);
        });
        return Response.status(Response.Status.OK).build();
    }


    public Response createAlbum(long userId, AppAlbum a, UriInfo uriInfo) {
        AppUser owner = getEm().find(AppUser.class,userId);
        if (owner==null) throw new NotFoundException();
        a.setOwner(owner);
        Response r = create(a,uriInfo);
        return r;
    }

    public Response getAlbums(long userId, UriInfo uriInfo, int limit, int offset) {
        EntityGraph graph = getEm().getEntityGraph("AppAlbum.albumFull");
        TypedQuery<AppAlbum> q = getEm().createNamedQuery("AppAlbum.findByOwner", AppAlbum.class);
        // alternative : query directe dans le code
        //TypedQuery<AppAlbum> q = getEm().createQuery("SELECT a FROM AppAlbum a WHERE a.owner.id=:id", AppAlbum.class);
        q.setHint("javax.persistence.fetchgraph", graph);
        q.setParameter("id",userId);
        return executeQuery(q,uriInfo,limit,offset);
    }


    public Response getAlbum(long userId, UriInfo uriInfo, int limit, int offset, long albumId) {
        EntityGraph graph = getEm().getEntityGraph("AppAlbum.albumFull");
        //TypedQuery<AppAlbum> q = getEm().createNamedQuery("AppAlbum.findByOwner", AppAlbum.class);
        // alternative : query directe dans le code
        TypedQuery<AppAlbum> q = getEm().createQuery("SELECT a FROM AppAlbum a WHERE a.owner.id=:id AND a.id=:albumId", AppAlbum.class);
        q.setHint("javax.persistence.fetchgraph", graph);
        q.setParameter("id",userId);
        q.setParameter("albumId",albumId);
        return executeQuery(q,uriInfo,limit,offset);
    }


    public Response deleteAlbum(long albumId) {
        Query q = getEm().createNativeQuery("DELETE FROM APPPHOTO WHERE OWNER_ID=?1");
        q.setParameter(1, albumId);
        q.executeUpdate();

        q = getEm().createNativeQuery("DELETE FROM APPALBUM WHERE id=?1");
        q.setParameter(1, albumId);
        q.executeUpdate();
        return Response.status(Response.Status.OK).build();
    }


    @Override
    public Response listAll(UriInfo uriInfo, int limit, int offset) {
        return super.executeQuery(getEm().createNamedQuery("AppAlbum.findAllEagerPhotos", AppAlbum.class),
                uriInfo,limit,offset);
    }
}

package fr.uga.miashs.cours4.dao;

import fr.uga.miashs.cours4.model.AppGroup;
import fr.uga.miashs.cours4.model.AppUser;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;


public class GroupsDao extends GenericJpaRestDao<AppGroup> {

    // Il y a une dépendance mutuelle entre GroupController et UserController
    // une solution est d'injecter Instance<UserController> à la place de  UserController
    // puis utiliser get() pour obtenir la référence
    @Inject
    private Instance<UsersDao> usersController;

    public GroupsDao() {
        super(AppGroup.class);
    }


    public Response addMembers(List<Long> ids, long groupId, UriInfo uriInfo) {
        // Comme l'égalité et le hashcode sont définis sur l'email
        // cela cause un select du membre dans tous les cas
        //AppUser membre = em.getReference(AppUser.class,userId);
        // la solution la plus efficace revient à faire un native insert
        // et traiter les éventuelles erreurs (duplicate, foreign key)
        Query q = getEm().createNativeQuery("INSERT INTO AppGroup_AppUser(AppGroup_id,Members_id) VALUES(?,?)");
        q.setParameter(1,groupId);
        ids.forEach( userId -> {
            q.setParameter(2,userId);
            q.executeUpdate();
            // La aussi meme avec em.getReference, il y a chargement des membres
            //em.find(AppGroup.class,g.id).getMembres().add(membre);
        });
        return Response.status(Response.Status.OK).build();
    }

    public Response addAlbums(List<Long> ids, long groupId, UriInfo uriInfo) {
        // Comme l'égalité et le hashcode sont définis sur l'email
        // cela cause un select du membre dans tous les cas
        //AppUser membre = em.getReference(AppUser.class,userId);
        // la solution la plus efficace revient à faire un native insert
        // et traiter les éventuelles erreurs (duplicate, foreign key)
        Query q = getEm().createNativeQuery("INSERT INTO AppGroup_AppAlbum(AppGroup_id,Albums_id) VALUES(?,?)");
        q.setParameter(1,groupId);
        ids.forEach( albumId -> {
            q.setParameter(2,albumId);
            q.executeUpdate();
            // La aussi meme avec em.getReference, il y a chargement des membres
            //em.find(AppGroup.class,g.id).getMembres().add(membre);
        });
        return Response.status(Response.Status.OK).build();
    }

    public Response deleteMembers(List<Long> ids, long groupId, UriInfo uriInfo) {
        // Comme l'égalité et le hashcode sont définis sur l'email
        // cela cause un select du membre dans tous les cas
        //AppUser membre = em.getReference(AppUser.class,userId);
        // la solution la plus efficace revient à faire un native insert
        // et traiter les éventuelles erreurs (duplicate, foreign key)
        Query q = getEm().createNativeQuery("DELETE FROM APPGROUP_APPUSER WHERE APPGROUP_ID=?1 AND MEMBERS_ID=?2");
        q.setParameter(1,groupId);
        ids.forEach( userId -> {
            q.setParameter(2,userId);
            q.executeUpdate();
            // La aussi meme avec em.getReference, il y a chargement des membres
            //em.find(AppGroup.class,g.id).getMembres().add(membre);
        });
        return Response.status(Response.Status.OK).build();
    }

    //Supprimer albums dans un group
    public Response deleteAlbums(List<Long> ids, long groupId, UriInfo uriInfo) {
        // Comme l'égalité et le hashcode sont définis sur l'email
        // cela cause un select du membre dans tous les cas
        //AppUser membre = em.getReference(AppUser.class,userId);
        // la solution la plus efficace revient à faire un native insert
        // et traiter les éventuelles erreurs (duplicate, foreign key)
        Query q = getEm().createNativeQuery("DELETE FROM APPGROUP_APPALBUM WHERE APPGROUP_ID=?1 AND ALBUMS_ID=?2");
        q.setParameter(1,groupId);
        ids.forEach( albumId -> {
            q.setParameter(2,albumId);
            q.executeUpdate();
            // La aussi meme avec em.getReference, il y a chargement des membres
            //em.find(AppGroup.class,g.id).getMembres().add(membre);
        });
        return Response.status(Response.Status.OK).build();
    }

    public Response getGroupOfMember(long memberId, UriInfo uriInfo) {
        // Comme l'égalité et le hashcode sont définis sur l'email
        // cela cause un select du membre dans tous les cas
        //AppUser membre = em.getReference(AppUser.class,userId);
        // la solution la plus efficace revient à faire un native insert
        // et traiter les éventuelles erreurs (duplicate, foreign key)
        Query q = getEm().createNativeQuery("SELECT g FROM APPGROUP g WHERE g.id in (SELECT APPGROUP_ID FROM APPGROUP_APPUSER WHERE MEMBERS_ID=?1)");
        q.setParameter(1,memberId);
        q.executeUpdate();
        return Response.status(Response.Status.OK).build();
    }


    public Response createGroup(long userId, AppGroup g, UriInfo uriInfo) {
        AppUser owner = getEm().find(AppUser.class,userId);
        if (owner==null) throw new NotFoundException();
        g.setOwner(owner);
        Response r = create(g,uriInfo);
        return r;
    }

    public Response getGroup(long userId,long groupId, UriInfo uriInfo, int limit, int offset) {
        EntityGraph graph = getEm().getEntityGraph("AppGroup.groupOnly");
        //TypedQuery<AppGroup> q = getEm().createNamedQuery("AppGroup.findByOwner", AppGroup.class);
        // alternative : query directe dans le code
        TypedQuery<AppGroup> q = getEm().createQuery("SELECT g FROM AppGroup g WHERE g.owner.id=:id and g.id=:group_id", AppGroup.class);
        q.setHint("javax.persistence.fetchgraph", graph);
        q.setParameter("id",userId);
        q.setParameter("group_id",groupId);
        return executeQuery(q,uriInfo,limit,offset);
    }

    public Response getGroups(long userId, UriInfo uriInfo, int limit, int offset) {
        EntityGraph graph = getEm().getEntityGraph("AppGroup.groupOnly");
        TypedQuery<AppGroup> q = getEm().createNamedQuery("AppGroup.findByOwner", AppGroup.class);
        // alternative : query directe dans le code
        //TypedQuery<AppGroup> q = getEm().createQuery("SELECT g FROM AppGroup g WHERE g.owner.id=:id", AppGroup.class);
        q.setHint("javax.persistence.fetchgraph", graph);
        q.setParameter("id",userId);
        return executeQuery(q,uriInfo,limit,offset);
    }



    @Override
    public Response listAll(UriInfo uriInfo, int limit, int offset) {
        return super.executeQuery(getEm().createNamedQuery("AppGroup.findAllEagerMembers", AppGroup.class),
                uriInfo,limit,offset);
    }
}
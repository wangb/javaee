package fr.uga.miashs.cours4.model;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;


@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(name="UNIQUE_OWNER_SOURCE", columnNames = {"source","owner_id"})
})
// les requêtes peuvent être définies ici sous forme de requête nommées
// et chargées dans le DAO via em.createNamedQuery(...)
// ou directement dans le DAO (c.f. PhotosDao). C'est une question de goût..
@NamedQueries({
        @NamedQuery(name="AppPhoto.findByOwner",
                query = "SELECT p FROM AppPhoto p WHERE p.owner.id=:id"),

        @NamedQuery(name="AppPhoto.photosOf",
                query="SELECT u FROM AppAlbum a INNER JOIN a.photos u WHERE a.id=:id")
})
// c'est pareil pour les EntityGraphs
@NamedEntityGraphs({
        // Exemple d'entity graph qui permet de ne charger que l'id et le nom d'un Album
        @NamedEntityGraph(name = "AppPhoto.photoOnly",
                attributeNodes = {@NamedAttributeNode("id"),@NamedAttributeNode("source")})
})


public class AppPhoto {
    @Id
    @GeneratedValue
    private long id;

    @NotBlank(message = "Vous devez selectionner une photo !")
    private String source;


    @ManyToOne
    @NotNull
    private AppAlbum owner;

    public AppPhoto(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @JsonbTransient
    public AppAlbum getOwner() {
        return owner;
    }

    public void setOwner(AppAlbum owner) {
        this.owner = owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppPhoto appPhoto = (AppPhoto) o;
        return id == appPhoto.id &&
                source.equals(appPhoto.source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, source);
    }
}

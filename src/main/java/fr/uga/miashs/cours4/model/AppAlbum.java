package fr.uga.miashs.cours4.model;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;


@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(name="UNIQUE_OWNER_NAME", columnNames = {"name","owner_id"})
})
// les requêtes peuvent être définies ici sous forme de requête nommées
// et chargées dans le DAO via em.createNamedQuery(...)
// ou directement dans le DAO (c.f. GroupsDao). C'est une question de goût..
@NamedQueries({
        @NamedQuery(name="AppAlbum.findByOwner",
                query = "SELECT a FROM AppAlbum a WHERE a.owner.id=:id"),
        @NamedQuery(name="AppAlbum.findAllEagerPhotos",
                query = "SELECT a FROM AppAlbum a LEFT JOIN FETCH a.photos"),
        @NamedQuery(name="AppAlbum.albumOf",
                query = "SELECT a FROM AppGroup g INNER JOIN g.albums a WHERE g.id=:id")
})
// c'est pareil pour les EntityGraphs
@NamedEntityGraphs({
        // Exemple d'entity graph qui permet de ne charger que l'id et le nom d'un Album
        @NamedEntityGraph(name = "AppAlbum.albumOnly",
                attributeNodes = {@NamedAttributeNode("id"),@NamedAttributeNode("name")}),
        @NamedEntityGraph(name = "AppAlbum.albumFull",
                attributeNodes = {@NamedAttributeNode("id"),@NamedAttributeNode("name"),
                        @NamedAttributeNode(value = "photos", subgraph = "photos-subgraph")},
                subgraphs = {
                        @NamedSubgraph(
                                name = "photos-subgraph",
                                attributeNodes = {
                                        @NamedAttributeNode("id"),
                                        @NamedAttributeNode("source")
                                }
                        )
                })
})

public class AppAlbum {

    @Id
    @GeneratedValue
    private long id;

    @NotBlank(message="Donner un Nom pour l'Album")
    private String name;

    //@NotBlank(message="Donner une Descritpion")
    private String description;

    @ManyToOne
    @NotNull
    private AppUser owner;

    @OneToMany(mappedBy = "owner")//, orphanRemoval=true, cascade = CascadeType.REMOVE)
    private Set<AppPhoto> photos;

    public AppAlbum(){}

    public AppAlbum(String name,  String description, AppUser owner, Set<AppPhoto> photos) {
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.photos = photos;
    }


    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AppUser getOwner() {
        return owner;
    }

    public void setOwner(AppUser owner) {
        this.owner = owner;
    }

    public Set<AppPhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<AppPhoto> photos) {
        this.photos = photos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppAlbum appAlbum = (AppAlbum) o;
        return name.equals(appAlbum.name) &&
                owner.equals(appAlbum.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, owner);
    }
}

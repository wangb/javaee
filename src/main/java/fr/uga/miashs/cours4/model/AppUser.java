package fr.uga.miashs.cours4.model;


import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import java.util.Objects;

// les requêtes peuvent être définies ici sous forme de requête nommées
// et chargées dans le DAO via em.createNamedQuery(...)
// ou directement dans le DAO (c.f. GroupsDao). C'est une question de goût..
@NamedQueries({
        @NamedQuery(name="AppUser.membersOf",
                query="SELECT u FROM AppGroup g INNER JOIN g.members u WHERE g.id=:id"),

        @NamedQuery(name = "AppUser.login",
                query = "SELECT u FROM AppUser u WHERE u.email=:email"),
})
@Entity
public class AppUser {

    @Id
    @GeneratedValue
    private long id;

    @NotBlank(message = "Le prénom doit être renseigné")
    private String firstname;
    @NotBlank(message = "Le nom doit être renseigné")
    private String lastname;

    @PositiveOrZero(message = "{appuser.agenegatif}")
    private int age;

    @NotBlank @Email @Column(unique=true)
    private String email;

    @NotBlank
    private String passwordHash;

    @Transient
    private String password;

    /*@OneToMany(mappedBy = "proprietaire")
    private Set<AppGroup> mesGroupes;*/

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "VARCHAR(5)")
    private AppUserType userType;



    public AppUser() { this.userType = AppUserType.USER; }

    public AppUser(long id, AppUserType type) {
        this.id = id;
        this.userType = type;
    }

    public AppUser(String firstname, String lastname, String email, int age) {
        super();
        this.firstname = firstname;
        this.lastname = lastname;
        this.email=email;
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    // À enlever si on ne veut pas retourner le hash du password à chaque nouvelle requête
    // c'est mieux
    @JsonbTransient
    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(long id){this.id = id; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppUser appUser = (AppUser) o;
        return Objects.equals(email,appUser.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getId() {
        return id;
    }

    public AppUserType getUserType(){ return userType; }
}

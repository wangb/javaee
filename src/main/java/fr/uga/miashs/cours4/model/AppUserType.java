package fr.uga.miashs.cours4.model;

public enum AppUserType {
    USER, ADMIN;
}

package fr.uga.miashs.cours4.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(name="UNIQUE_OWNER_NAME", columnNames = {"name","owner_id"})
})
// les requêtes peuvent être définies ici sous forme de requête nommées
// et chargées dans le DAO via em.createNamedQuery(...)
// ou directement dans le DAO (c.f. GroupsDao). C'est une question de goût..
@NamedQueries({
        @NamedQuery(name="AppGroup.findByOwner",
                query = "SELECT g FROM AppGroup g WHERE g.owner.id=:id"),
        @NamedQuery(name="AppGroup.findAllEagerMembers",
                query = "SELECT g FROM AppGroup g LEFT JOIN FETCH g.members"),

        @NamedQuery(name="AppGroup.deleteByOwner",
                //query = "SELECT g FROM AppGroup g WHERE g.owner.id=:id"
                query = "DELETE FROM AppGroup g WHERE g.owner.id=:id"),
})
// c'est pareil pour les EntityGraphs
@NamedEntityGraphs({
        // Exemple d'entity graph qui permet de ne charger que l'id et le nom d'un groupe
        @NamedEntityGraph(name = "AppGroup.groupOnly",
                attributeNodes = {@NamedAttributeNode("id"),@NamedAttributeNode("name")})
})
public class AppGroup {

    @Id @GeneratedValue
    private long id;

    @NotBlank(message="A name for the group must be given")
    private String name;

    @ManyToOne
    @NotNull
    private AppUser owner;

    @ManyToMany
    private Set<AppUser> members;

    @ManyToMany
    private Set<AppAlbum> albums;

    public AppGroup() {}

    public AppGroup(String name, AppUser owner) {
        this.name = name;
        this.owner = owner;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AppUser getOwner() {
        return owner;
    }

    public void setOwner(AppUser owner) {
        this.owner = owner;
    }

    public Set<AppUser> getMembers() {
        return members;
    }

    public void setMembers(Set<AppUser> members) {
        this.members = members;
    }

    public Set<AppAlbum> getAlbums() {
        return albums;
    }

    public void setAlbums(Set<AppAlbum> albums) {
        this.albums = albums;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppGroup appGroup = (AppGroup) o;
        return Objects.equals(name, appGroup.name) &&
                Objects.equals(owner, appGroup.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, owner);
    }
}

package fr.uga.miashs.cours4.controllers;

import fr.uga.miashs.cours4.dao.AlbumsDao;
import fr.uga.miashs.cours4.dao.GroupsDao;
import fr.uga.miashs.cours4.dao.PhotosDao;
import fr.uga.miashs.cours4.dao.UsersDao;
import fr.uga.miashs.cours4.model.AppAlbum;
import fr.uga.miashs.cours4.model.AppGroup;
import fr.uga.miashs.cours4.model.AppPhoto;
import fr.uga.miashs.cours4.model.AppUser;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;


@Path("/albums")
public class AlbumsController {


    // On  peut injecter le JWT
    /*@Inject
    private JsonWebToken callerPrincipal;*/

    /*@Inject
    @Claim(standard = Claims.raw_token)
    private String rawToken;*/

    /*@Inject
    @Claim(standard = Claims.upn)
    private ClaimValue<String> emailUPN;*/

    @Inject
    private AlbumsDao albumsDao;

    @Inject
    private UsersDao usersDao;

    @Inject
    private PhotosDao photosDao;


    @POST
    @Path("/{id: [0-9]+}/photos")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createPhoto(@PathParam("id") long albumId, AppPhoto p, @Context UriInfo uriInfo) {
        return photosDao.createPhoto(albumId, p, uriInfo);
    }



    @POST
    @Path("/{Id: [0-9]+}/photos")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPhotos(List<Long> ids, @PathParam("Id") long albumId, @Context UriInfo uriInfo) {
        return albumsDao.addPhotos(ids, albumId, uriInfo);
    }

    @GET
    @Path("/{id: [0-9]+}/photos")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getPhotos(@PathParam("id") long albumId, @Context UriInfo uriInfo,
                              @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return photosDao.getPhotos(albumId, uriInfo, limit, offset);
    }


    @GET
    @Path("/{id: [0-9]+}/photos/{photoId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getPhoto(@PathParam("id") long albumId, @Context UriInfo uriInfo,
                             @PathParam("limit") int limit, @PathParam("offset") int offset, @PathParam("photoId") long photoId ) {
        return photosDao.getPhoto(albumId, uriInfo, limit, offset, photoId);
    }


    //@RolesAllowed("admin")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return albumsDao.listAll(uriInfo, limit, offset);
    }


    @DELETE
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteAlbum(@PathParam("id") long id) {
        return albumsDao.deleteAlbum(id);
    }



}

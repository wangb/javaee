package fr.uga.miashs.cours4.controllers;

import fr.uga.miashs.cours4.dao.AlbumsDao;
import fr.uga.miashs.cours4.dao.PhotosDao;
import fr.uga.miashs.cours4.model.AppPhoto;
import fr.uga.miashs.sempic.rdf.BasicSempicRDFStore;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;


@Path("/photos")
public class PhotosController {

    @Inject
    private AlbumsDao albumsDao;

    @Inject
    private PhotosDao photosDao;

    private BasicSempicRDFStore rdfStore = new BasicSempicRDFStore();
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return photosDao.listAll(uriInfo, limit, offset);
    }


    @POST
    @Path("/{photoId: [0-9]+}/depicts")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addDepicts(@PathParam("photoId") long photoId, List<String> resources) {
        String res = photosDao.addDepicts(photoId, resources);
        return Response.ok(res).build();
    }

    @POST
    @Path("/{photoId: [0-9]+}/author")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response hasAuthor(@PathParam("photoId") long photoId, List<String> resources) {
        String res = photosDao.hasAuthor(photoId, resources);
        return Response.ok(res).build();
    }

    @POST
    @Path("/{photoId: [0-9]+}/event")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEvent(@PathParam("photoId") long photoId, List<String> resources) {
        String res = photosDao.addEvent(photoId, resources);
        return Response.ok(res).build();
    }

    @POST
    @Path("/{photoId: [0-9]+}/date")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addDate(@PathParam("photoId") long photoId, String date) {
        String res = photosDao.addDate(photoId, date);
        return Response.ok(res).build();
    }

    @POST
    @Path("/{photoId: [0-9]+}/location")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addLocation(@PathParam("photoId") long photoId, String location) {
        String res = photosDao.addLocation(photoId, location);
        return Response.ok(res).build();
    }

    @POST
    @Path("/{photoId: [0-9]+}/title")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addTitle(@PathParam("photoId") long photoId, String title) {
        String res = photosDao.addTitle(photoId, title);
        return Response.ok(res).build();
    }


    @DELETE
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deletePhoto(@PathParam("id") long id) {
        rdfStore.deletePhoto(id);
        return photosDao.delete(id);
    }
}

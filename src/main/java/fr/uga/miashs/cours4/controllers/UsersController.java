package fr.uga.miashs.cours4.controllers;

import fr.uga.miashs.cours4.dao.AlbumsDao;
import fr.uga.miashs.cours4.dao.GroupsDao;
import fr.uga.miashs.cours4.dao.UsersDao;
import fr.uga.miashs.cours4.model.AppAlbum;
import fr.uga.miashs.cours4.model.AppGroup;
import fr.uga.miashs.cours4.model.AppUser;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.Map;

@Path("/users")
public class UsersController {

    @Inject
    private GroupsDao groupsDao;

    @Inject
    private UsersDao usersDao;

    @Inject
    private AlbumsDao albumsDao;

    @GET
    @Path("/login")
    public Response authenticate(@QueryParam("email") String email, @QueryParam("passwd") String password) {
        return usersDao.authenticate(email, password);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(AppUser obj, @Context UriInfo uriInfo) {
        return usersDao.create(obj, uriInfo);
    }

    @GET
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get a user", description = "retourne les informations sur l’utilisateur id")
    @APIResponse(responseCode = "400", description = "No books found")
    @Tag(name = "BETA", description = "This API is currently in beta state")
    public AppUser read(@PathParam("id") long id) {
        return usersDao.read(id);
    }

    @PATCH
    //@PUT
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") long id, Map<String, Object> obj, @Context UriInfo uriInfo) {
        return usersDao.update(id, obj, uriInfo);
    }
/*
    @DELETE
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") long id) {
        return usersDao.delete(id);
    }


 */

    @DELETE
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("id") long id) {
        return usersDao.deleteUser(id);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return usersDao.listAll(uriInfo, limit, offset);
    }


    @POST
    @Path("/{id: [0-9]+}/groups")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createGroup(@PathParam("id") long userId, AppGroup g, @Context UriInfo uriInfo) {
        return groupsDao.createGroup(userId, g, uriInfo);
    }

    @GET
    @Path("/{id: [0-9]+}/groups")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGroups(@PathParam("id") long userId, @Context UriInfo uriInfo,
                              @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return groupsDao.getGroups(userId, uriInfo, limit, offset);
    }

    /*
    @GET
    @Path("/{id: [0-9]+}/ingroup")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGroupOfMember(@PathParam("id") long memberId, @Context UriInfo uriInfo) {
        return groupsDao.getGroupOfMember(memberId, uriInfo);
    }

     */


    @GET
    @Path("/{id: [0-9]+}/groups/{groupId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGroup(@PathParam("id") long userId,@PathParam("groupId") long groupId, @Context UriInfo uriInfo,
                             @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return groupsDao.getGroup(userId,groupId, uriInfo, limit, offset);
    }



    @POST
    @Path("/{id: [0-9]+}/albums")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAlbum(@PathParam("id") long userId, AppAlbum a, @Context UriInfo uriInfo) {
        return albumsDao.createAlbum(userId, a, uriInfo);
    }

    @GET
    @Path("/{id: [0-9]+}/albums")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAlbums(@PathParam("id") long userId, @Context UriInfo uriInfo,
                              @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return albumsDao.getAlbums(userId, uriInfo, limit, offset);
    }


    @GET
    @Path("/{id: [0-9]+}/albums/{albumId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAlbum(@PathParam("id") long userId, @Context UriInfo uriInfo,
                              @PathParam("limit") int limit, @PathParam("offset") int offset, @PathParam("albumId") long albumId) {
        return albumsDao.getAlbum(userId, uriInfo, limit, offset, albumId);
    }


    /*
    @DELETE
    @Path("/{id: [0-9]+}/albums")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteAlbum(@PathParam("id") long id) {
        return albumsDao.delete(id);
    }
    */

}

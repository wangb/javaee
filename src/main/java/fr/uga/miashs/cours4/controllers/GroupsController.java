package fr.uga.miashs.cours4.controllers;

import fr.uga.miashs.cours4.dao.GroupsDao;
import fr.uga.miashs.cours4.dao.UsersDao;
import fr.uga.miashs.cours4.dao.AlbumsDao;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("/groups")
public class GroupsController {

    // On  peut injecter le JWT
    /*@Inject
    private JsonWebToken callerPrincipal;*/

    /*@Inject
    @Claim(standard = Claims.raw_token)
    private String rawToken;*/

    /*@Inject
    @Claim(standard = Claims.upn)
    private ClaimValue<String> emailUPN;*/

    @Inject
    private GroupsDao groupsDao;

    @Inject
    private UsersDao usersDao;

    @Inject
    private AlbumsDao albumsDao;


    //@RolesAllowed("admin")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return groupsDao.listAll(uriInfo, limit, offset);
    }

    @GET
    @Path("/{groupId: [0-9]+}/members")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGroupMembers(@PathParam("groupId") long groupId,@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return usersDao.getGroupMembers(groupId,uriInfo, limit, offset);
    }

    @POST
    @Path("/{groupId: [0-9]+}/members")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addMembers(List<Long> ids, @PathParam("groupId") long groupId, @Context UriInfo uriInfo) {
        return groupsDao.addMembers(ids, groupId, uriInfo);
    }

    @POST
    @Path("/{groupId: [0-9]+}/albums")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addAlbums(List<Long> ids, @PathParam("groupId") long groupId, @Context UriInfo uriInfo) {
        return groupsDao.addAlbums(ids, groupId, uriInfo);
    }

    @GET
    @Path("/{groupId: [0-9]+}/albums")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGroupAlbums(@PathParam("groupId") long groupId,@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return albumsDao.getGroupAlbums(groupId,uriInfo, limit, offset);
    }
/*
    @POST
    @Path("/{id: [0-9]+}/members/{userId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addMembers(@PathParam("userId") long userId, @PathParam("id") long groupId, @Context UriInfo uriInfo)
    {
        return groupsDao.addMembers(List.of(userId), groupId, uriInfo);
    }

 */



    @DELETE
    @Path("/{id: [0-9]+}/members/{userId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteMembers(@PathParam("userId") long userId, @PathParam("id") long groupId, @Context UriInfo uriInfo)
    {
        return groupsDao.deleteMembers(List.of(userId), groupId, uriInfo);
    }

    @DELETE
    @Path("/{id: [0-9]+}/albums/{albumId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteAlbums(@PathParam("albumId") long albumId, @PathParam("id") long groupId, @Context UriInfo uriInfo)
    {
        return groupsDao.deleteAlbums(List.of(albumId), groupId, uriInfo);
    }

    @DELETE
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteGroup(@PathParam("id") long id) {
        return groupsDao.delete(id);
    }
}

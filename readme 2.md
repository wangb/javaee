# MicroProfile generated Application

## Introduction

MicroProfile Starter has generated this MicroProfile application for you.

The generation of the executable jar file can be performed by issuing the following command

    mvn clean package

This will create an executable jar file **cours2-exec.jar** within the _target_ maven folder. This can be started by executing the following command

    java -jar target/cours2-exec.jar

Another option is to clean, build and launch with a single command

	mvn clean package tomee:run

Then you can modify the code and use the build from the ide (without restarting) or

    mvn build

To launch the test page, open your browser at the following URL

    http://localhost:8080/index.html
    
To access the database with a basic graphical tool

    java -cp target/apache-tomee/lib/hsqldb-2.3.2.jar org.hsqldb.util.DatabaseManagerSwing
    
In the field URL choose

    jdbc:hsqldb:file:./target/app_database;hsqldb.lock_file=false

## Specification examples

By default, there is always the creation of a JAX-RS application class to define the path on which the JAX-RS endpoints are available.

Also, a simple Hello world endpoint is created, have a look at the class **HelloController**.

More information on MicroProfile can be found [here](https://microprofile.io/)


### Config

Configuration of your application parameters. Specification [here](https://microprofile.io/project/eclipse/microprofile-config)

The example class **ConfigTestController** shows you how to inject a configuration parameter and how you can retrieve it programmatically.

### Some curl requests
	curl -i http://localhost:8080/data/users
	curl -i http://localhost:8080/data/users/2
	curl -i -X POST -H "Content-Type: application/json" -d '{"email":"wang@wang.org","age":24,"firstname":"biyun","lastname":"wang","password":"testest"}' http://localhost:8080/data/users
	curl -i -X POST -H "Content-Type: application/json" -d '{"email":"diallo@diallo.org","age":18,"firstname":"bob","lastname":"dylan","password":"testest"}' http://localhost:8080/data/users
	curl -i -X POST -H "Content-Type: application/json" -d '{"email":"titi@titi.org","age":18,"firstname":"bob","lastname":"dylan","password":"testest"}' http://localhost:8080/data/users
    curl -i -X DELETE http://localhost:8080/data/users/1 //quand il est dans un groupe on peux pas lui supprimer pour l'instant car il faut supprimer le groupe d'abord et il n'y a pas de méthode pour ça
   
    curl -i -X POST -H "Content-Type: application/json" -d '{"email":"diouldeo@dioulde.fr","age":24,"firstname":"diouldé","lastname":"diallo","password":"testest"}' http://localhost:8080/data/users
    
    curl -i -X GET http://localhost:8080/data/users/login\?email\=wang@wang\.org\&passwd=testest    //Auth-login

### Groups Query    
    curl -i -X POST -H "Content-Type: application/json" -d '{"name":"wic"}' http://localhost:8080/data/users/1/groups  //ajouter un groupe
    curl -i -X GET http://localhost:8080/data/groups
    curl -i -X GET http://localhost:8080/data/users/1/groups
    curl -i -X DELETE http://localhost:8080/data/groups/9
    
    curl -i -X GET http://localhost:8080/data/users/4/ingroup
### Members Query
    curl -i -X POST -H "Content-Type: application/json" -d '[4,5]' http://localhost:8080/data/groups/3/members
    curl -i -X POST http://localhost:8080/data/groups/3/members/2
    curl -i -X GET http://localhost:8080/data/groups/10/members
    curl -i -X DELETE http://localhost:8080/data/groups/10/members/5
    
### Albums Query 
    curl -i -X POST -H "Content-Type: application/json" -d '{"name":"Sempic", "description": "sempic ontologie"}' http://localhost:8080/data/users/1/albums  //ajouter un album
    curl -i -X GET http://localhost:8080/data/albums                 // Tous les Albums de 
    curl -i -X GET http://localhost:8080/data/users/1/albums/       // Tous les Albums du User 1
    curl -i -X GET http://localhost:8080/data/users/1/albums/3      // L'Album 3 du User 1
    curl -i -X DELETE http://localhost:8080/data/albums/3           // Supprimer un Album 
   ## Albums dans Groups (partagé)
    curl -i -X POST -H "Content-Type: application/json" -d '[8]' http://localhost:8080/data/groups/3/albums   //ajouter album dans group
    curl -i -X GET http://localhost:8080/data/groups/3/albums
    curl -i -X DELETE http://localhost:8080/data/groups/3/albums/8
    
    
### Photos Query 
    curl -i -X POST -H "Content-Type: application/json" -d '{"source":"Manuel with Jérôme"}' http://localhost:8080/data/albums/2/photos  
    curl -i -X GET http://localhost:8080/data/photos
    curl -i -X GET http://localhost:8080/data/albums/2/photos
    curl -i -X GET http://localhost:8080/data/albums/3/photos/12
    curl -i -X DELETE http://localhost:8080/data/photos/12
   ### add depicts
    curl -i -X POST -H "Content-Type: application/json" -d '["http://miashs.univ-grenoble-alpes.fr/depicts/6", "http://miashs.univ-grenoble-alpes.fr/depicts/1"]' http://localhost:8080/data/photos/9/depicts
   ### add Author
    curl -i -X POST -H "Content-Type: application/json" -d '["http://miashs.univ-grenoble-alpes.fr/author/1"]' http://localhost:8080/data/photos/9/author
   ### add Event
    curl -i -X POST -H "Content-Type: application/json" -d '["http://miashs.univ-grenoble-alpes.fr/event/1"]' http://localhost:8080/data/photos/9/event
   ### add Date
    curl -i -X POST -H "Content-Type: application/json" -d '2014-09-02' http://localhost:8080/data/photos/9/date
   ### add Location
    curl -i -X POST -H "Content-Type: application/json" -d 'Grenoble' http://localhost:8080/data/photos/9/location
   ### add Title
    curl -i -X POST -H "Content-Type: application/json" -d 'Manuel with Jérôme' http://localhost:8080/data/photos/9/title
                     
### Liste de commande
    curl -i -X POST -H "Content-Type: application/json" -d '{"email":"wang@wang.org","age":24,"firstname":"biyun","lastname":"wang"}' http://localhost:8080/data/users
    curl -i -X POST -H "Content-Type: application/json" -d '{"name":"Neige Photos", "description": "Jour de la neige"}' http://localhost:8080/data/users/1/albums
    curl -i -X POST -H "Content-Type: application/json" -d '{"source":"Photos"}' http://localhost:8080/data/albums/2/photos
    curl -i -X GET http://localhost:8080/data/users/1/albums


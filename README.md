

# MicroProfile generated Application

## Introduction

MicroProfile Starter has generated this MicroProfile application for you.

The generation of the executable jar file can be performed by issuing the following command

    mvn clean package

This will create an executable jar file **cours2-exec.jar** within the _target_ maven folder. This can be started by executing the following command

    java -jar target/cours2-exec.jar

Another option is to clean, build and launch with a single command

	mvn clean package tomee:run

Then you can modify the code and use the build from the ide (without restarting) or

    mvn build

To launch the test page, open your browser at the following URL

    http://localhost:8080/index.html
    
To access the database with a basic graphical tool

    java -cp target/apache-tomee/lib/hsqldb-2.3.2.jar org.hsqldb.util.DatabaseManagerSwing
    
In the field URL choose

    jdbc:hsqldb:file:./target/app_database;hsqldb.lock_file=false

## Specification examples

By default, there is always the creation of a JAX-RS application class to define the path on which the JAX-RS endpoints are available.

Also, a simple Hello world endpoint is created, have a look at the class **HelloController**.

More information on MicroProfile can be found [here](https://microprofile.io/)


### Config

Configuration of your application parameters. Specification [here](https://microprofile.io/project/eclipse/microprofile-config)

The example class **ConfigTestController** shows you how to inject a configuration parameter and how you can retrieve it programmatically.

### Some curl requests
	curl -i http://localhost:8080/data/users        // Récupérer tous les utilisateurs 
	curl -i http://localhost:8080/data/users/userId     // Récupérer l'utilisateur (userId)
	curl -i -X POST -H "Content-Type: application/json" -d '{"email":"wang@wang.org","age":24,"firstname":"biyun","lastname":"wang","password":"testest"}' http://localhost:8080/data/users     // Créer un Utilisateur
	curl -i -X POST -H "Content-Type: application/json" -d '{"email":"diallo@diallo.org","age":18,"firstname":"bob","lastname":"dylan","password":"testest"}' http://localhost:8080/data/users
	curl -i -X POST -H "Content-Type: application/json" -d '{"email":"titi@titi.org","age":18,"firstname":"bob","lastname":"dylan","password":"testest"}' http://localhost:8080/data/users
    curl -i -X DELETE http://localhost:8080/data/users/userId //quand il est dans un groupe on peux pas lui supprimer pour l'instant car il faut supprimer le groupe d'abord et il n'y a pas de méthode pour ça
       
    curl -i -X GET http://localhost:8080/data/users/login\?email\=wang@wang\.org\&passwd=testest    // Pour Connecter un utilisateur avec email (wang@wang.org) et password (testest)

### Groups Query    
    curl -i -X POST -H "Content-Type: application/json" -d '{"name":"wic"}' http://localhost:8080/data/users/userId/groups  //ajouter un groupe pour l'utilisateur (userId)
    curl -i -X GET http://localhost:8080/data/groups    // Récupérer tous les groupes
    curl -i -X GET http://localhost:8080/data/users/userId/groups    // Récupérér les groupes de l'utilisateur (userId)
    curl -i -X DELETE http://localhost:8080/data/groups/groupId   // Supprimer le groupe (groupId)
    
    
### Members Query
    curl -i -X POST -H "Content-Type: application/json" -d '[userId1,userId2,userIdn]' http://localhost:8080/data/groups/groupId/members // Ajouter des membres (userId1,userId2, userIdn) dans le groupe (groupId)
    curl -i -X GET http://localhost:8080/data/groups/groupId/members      // Récupérer tous les membres du groupe (groupId)
    curl -i -X DELETE http://localhost:8080/data/groups/groupId/members/userId     // Supprimer un membre (userId) dans le groupe (groupId)
    
### Albums Query 
    curl -i -X POST -H "Content-Type: application/json" -d '{"name":"Sempic", "description": "sempic ontologie"}' http://localhost:8080/data/users/userId/albums  //ajouter un album pour l'utilisateur (userId)
    curl -i -X GET http://localhost:8080/data/albums                             // Récupérer tous les Albums
    curl -i -X GET http://localhost:8080/data/users/userId/albums               // Récupérer tous les Albums de l'utilisateur (userId)
    curl -i -X GET http://localhost:8080/data/users/1/albums/albumId            // Récupérer l'album (albumId) de l'utilisateur (userId)
    curl -i -X DELETE http://localhost:8080/data/albums/albumId                 // Supprimer l'Album (albumId)
   ## Albums dans Groups (partagé)
    curl -i -X POST -H "Content-Type: application/json" -d '[8]' http://localhost:8080/data/groups/3/albums   // ajouter des albums dans un groupe
     curl -i -X POST -H "Content-Type: application/json" -d '[albumId1,albumId2,albumIdn]' http://localhost:8080/data/groups/groupId/albums   //ajouter des albums dans un groupe
    curl -i -X GET http://localhost:8080/data/groups/groupId/albums   // Récupérer les albums du groupe (groupId)
    curl -i -X DELETE http://localhost:8080/data/groups/groupId/albums/albumId  // Supprimer un album (albumId) du groupe (groupId)
    
    
### Photos Query 
    curl -i -X POST -H "Content-Type: application/json" -d '{"source":"WIC"}' http://localhost:8080/data/albums/albumId/photos  // Ajouter une photo dans l'album (albumId)
    curl -i -X GET http://localhost:8080/data/photos                                 // Récupérer toutes les photos
    curl -i -X GET http://localhost:8080/data/albums/albumId/photos                  // Récupérer toutes les photos de l'album (albumId)
    curl -i -X GET http://localhost:8080/data/albums/albumId/photos/photoId          // Récupérer la photo (photoId) de l'album (albumId)
    curl -i -X DELETE http://localhost:8080/data/photos/photoId                      // Supprimer la photo (photoId)
   ### add depicts
    [
    dans sparql:
    INSERT DATA {  
        <http://miashs.univ-grenoble-alpes.fr/depicts/1> a sempic:PersonInPicture.  
        <http://miashs.univ-grenoble-alpes.fr/depicts/1> rdfs:label "Manuel". 
        }
    }
    curl -i -X POST -H "Content-Type: application/json" -d '["http://miashs.univ-grenoble-alpes.fr/depicts/depictId1", "http://miashs.univ-grenoble-alpes.fr/depicts/depictId2"]' http://localhost:8080/data/photos/photoId/depicts
   ### add Author
    curl -i -X POST -H "Content-Type: application/json" -d '["http://miashs.univ-grenoble-alpes.fr/author/authorId"]' http://localhost:8080/data/photos/photoId/author
   ### add Event
    curl -i -X POST -H "Content-Type: application/json" -d '["http://miashs.univ-grenoble-alpes.fr/event/1"]' http://localhost:8080/data/photos/photoId/event
   ### add Date
    curl -i -X POST -H "Content-Type: application/json" -d '2014-09-02' http://localhost:8080/data/photos/photoId/date
   ### add Location
    curl -i -X POST -H "Content-Type: application/json" -d 'Marseille' http://localhost:8080/data/photos/photoId/location
   ### add Title
    curl -i -X POST -H "Content-Type: application/json" -d 'M2 WIC Party' http://localhost:8080/data/photos/photoId/title
                     


